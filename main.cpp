// Project 2 - Rob Johnson - pcc cs162 - 10/14/18

#include <iostream>
#include <fstream>

using namespace std;

#define MAX_TASKS 200
#define MAX_STRING 250
#define INPUT_FILENAME ("tasks.txt")
#define OUTPUT_FILENAME ("tasks.txt")
#define YEAR_UPPER_LIMIT (2078)
#define YEAR_LOWER_LIMIT (2017)
#define MONTH_UPPER_LIMIT (12)
#define MONTH_LOWER_LIMIT (1)
#define DAY_UPPER_LIMIT (31)
#define DAY_LOWER_LIMIT (1)

struct Task
{
    char name[MAX_STRING];
    char description[MAX_STRING];
    int day;
    int month;
    int year;
    bool complete;
};


// date comparison function
// inputs: year, month, day, task
// returns -1 if the date in the task is earlier than the supplied date
// returns 0 if the date in the task is the same as the supplied date
// returns 1 if the date in the task is late than the supplied date
int date_compare(int year, int month, int day, struct Task task)
{
    int result;

    if(task.year < year)
    {
        result = -1;
    }
    else if(task.year > year)
    {
        result = 1;
    }
    else // task.year == year
    {
        if(task.month < month)
        {
            result = -1;
        }
        else if(task.month > month)
        {
            result = 1;
        }
        else // task.month == month
        {
            if(task.day < day)
            {
                result = -1;
            }
            else if(task.day > day)
            {
                result = 1;
            }
            else // task.day == day
            {
                result = 0;
            }
        }
    }

    return result;
}


// tests to determine whether a task falls within a provided date range
bool date_within_range(int year0, int month0, int day0, int year1, int month1, int day1, struct Task task)
{
    // by adding the result of date_compare for each of the dates and taking the absolute value of the result
    // it is possible to determine whether the date is within the range, regardless of the order in which the date range is provided
    // if result == 0 then the task date is within the range (or both dates are the same and task date is also the same)
    // if result == 1 then the task date falls on one of the dates
    // if result == 2 then the task is earlier or later than both of the dates and is not valid
    bool result;
    int sum = date_compare(year0, month0, day0, task) + date_compare(year1, month1, day1, task);

    if(sum > -2 && sum < 2)
    {
        result = true;
    }
    else
    {
        result = false;
    }

    return result;
}


// prints the task which is provided as an argument
void print_task(struct Task task)
{
    cout << "------------------------" << endl;
    cout << "Name: " << task.name << endl;
    cout << "Description: " << task.description << endl;
    cout << "Date: " << task.month << "/" << task.day << "/" << task.year << endl;
    if(task.complete)
    {
        cout << "Task is complete." << endl;
    }
    else
    {
        cout << "Task is NOT complete." << endl;
    }
    cout << "------------------------" << endl;
}


// print all tasks
void print_all(const struct Task task[MAX_TASKS], int task_count)
{
    for(int i = 0; i < task_count; ++i)
    {
        print_task(task[i]);
    }
}


void input_number(int& number, int upper_limit, int lower_limit)
{
    number = 0;

    while(number == 0)
    {
        cin >> number;
        while(cin.fail() || number < lower_limit || number > upper_limit)
        {
            cin.clear();
            cin.ignore(100, '\n');
            cout << "Invalid entry. Should be an integer between " << lower_limit << " and " << upper_limit << endl;
            cin >> number;
        }
    }
}


void input_date(int& year, int& month, int& day)
{
    cout << "Enter year: ";
    input_number(year, YEAR_UPPER_LIMIT, YEAR_LOWER_LIMIT);

    cout << "Enter month: ";
    input_number(month, MONTH_UPPER_LIMIT, MONTH_LOWER_LIMIT);

    cout << "Enter day: ";
    input_number(day, DAY_UPPER_LIMIT, DAY_LOWER_LIMIT);
}


void print_range(const struct Task task[MAX_TASKS], int task_count)
{
    int year0, year1, month0, month1, day0, day1;
    cout << "Enter the first date." << endl;
    input_date(year0, month0, day0);
    cout << "Enter the second date." << endl;
    input_date(year1, month1, day1);

    for(int i = 0; i < task_count; ++i)
    {
        if(date_within_range(year0, month0, day0, year1, month1, day1, task[i]))
        {
            print_task(task[i]);
        }
    }
}


void add_task(struct Task *task, int& len)
{
    cin.ignore(MAX_STRING,'\n');

    cout << "Enter task name: ";
    cin.getline(task[len].name, MAX_STRING);

    cout << "Enter task description: ";
    cin.getline(task[len].description, MAX_STRING);

    input_date(task[len].year, task[len].month, task[len].day);

    task[len].complete = false;  // Always setting to false b/c it wouldn't make sense to add a completed item to the list.

    ++len;
}


// returns the number of entries that have been read
int fileRead(struct Task *tasks)
{
    int len = 0;
    ifstream infile;

    infile.open(INPUT_FILENAME);
    if(infile)
    {
        infile.peek(); // tests for end of file w/o moving the read pointer ahead
        for(int i = 0; !infile.eof(); ++i)
        {
            infile.getline(tasks[i].name, MAX_STRING);

            infile.getline(tasks[i].description, MAX_STRING);

            infile >> tasks[i].month;
            infile.ignore(MAX_STRING,'\n');

            infile >> tasks[i].day;
            infile.ignore(MAX_STRING,'\n');

            infile >> tasks[i].year;
            infile.ignore(MAX_STRING,'\n');

            infile >> tasks[i].complete;
            infile.ignore(MAX_STRING,'\n');

            ++len;

            infile.peek();
        }
        infile.close();
    }
    else
    {
        cout << "Error opening input file." << endl;
    }

    return len;
}


void fileWrite(struct Task *tasks, int len)
{
    ofstream outfile;

    outfile.open(OUTPUT_FILENAME);

    for(int i = 0; i < len; ++i)
    {
        outfile << tasks[i].name << endl;
        outfile << tasks[i].description << endl;
        outfile << tasks[i].month << endl;
        outfile << tasks[i].day << endl;
        outfile << tasks[i].year << endl;
        outfile << tasks[i].complete << endl;
    }

    outfile.close();
}


int main(void)
{
    int task_count = 0;
    struct Task task[MAX_TASKS];
    bool repeat;

    cout << "******* Task Manager App *******" << endl << endl;

    task_count = fileRead(task);    // Load tasks from file

    do
    {
        char response;
        repeat = true;

        cout << endl << "********MAIN MENU**********\n1 - Print All Tasks\n2 - Print Tasks in Date Range\n3 - Add Task\n4 - quit\nEnter choice: ";
        cin >> response;

        switch(response)
        {
            case '1':
                print_all(task, task_count);
                break;
            case '2':
                print_range(task, task_count);
                break;
            case '3':
                add_task(task, task_count);
                break;
            case '4':
                repeat = false;
                break;
            default:
                cout << "Invalid entry. Please try again with a valid input." << endl; // todo handle invalid + excess + purge extra
                break;
        }
    } while(repeat == true);

    fileWrite(task, task_count);    // save tasks to file.

    return 0;
}
